#include <getopt.h>
#include <mpi.h>

#include <cfloat>
#include <climits>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <string>

#include <vector>

#include "mpi_util.h"
#include "grid.h"

using parsys::MpiRequestArray;
using parsys::MpiProc;

using std::array;
using std::vector;

namespace
{

constexpr int MASTER_PROC = 0;
constexpr int MAX_TASKS = 49;

struct Args
{
	size_t height;
	size_t width;

	long steps;
	long converge_check_every;

	int procs_sqrt;

	float cx;
	float cy;

	Args()
		: height(1000)
		, width(1000)
		, steps(1000)
		, converge_check_every(LONG_MAX)
		, cx(0.1)
		, cy(0.1)
	{
	}
};

struct MpiAllInfo
{
	MpiProc proc;

	size_t neighbor_count;
	bool use_first;

	long steps;
	long converge_check_every;

	float cx;
	float cy;

	Grid first;
	Grid second;

	MpiRequestArray first_send_reqs;
	MpiRequestArray first_recv_reqs;

	MpiRequestArray second_send_reqs;
	MpiRequestArray second_recv_reqs;

	MpiAllInfo()
		: neighbor_count(0)
		, use_first(true)
	{
	}
};

void print_grid(const Grid& grid, const char* outfile)
{
	FILE* out = fopen(outfile, "w");
	if (out == nullptr) {
		perror("File creation failed");
		return;
	}

	for (size_t i = 0; i < grid.width() + 2; i++) {
		fprintf(out, "%6.1f%c", 0.0, i == grid.width() + 1 ? '\n' : ' ');
	}

	for (size_t i = 0; i < grid.height(); i++) {
		fprintf(out, "%6.1f ", 0.0);

		for (size_t j = 0; j < grid.width(); j++) {
			fprintf(out, "%6.1f ", grid(i, j));
		}

		fprintf(out, "%6.1f\n", 0.0);
	}

	for (size_t i = 0; i < grid.width() + 2; i++) {
		fprintf(out, "%6.1f%c", 0.0, i == grid.width() + 1 ? '\n' : ' ');
	}

	fclose(out);
}

Args parse_args(int argc, char** argv, int comm_rank, int comm_size)
{
	using std::isfinite;
	using std::stof;
	using std::stoll;
	using std::stol;

	if (comm_size > MAX_TASKS) {
		if (comm_rank == MASTER_PROC) {
			fprintf(stderr, "Too many tasks, max allowed is %d\n", MAX_TASKS);
		}

		MPI_Finalize();
		exit(EXIT_FAILURE);
	}

	Args args;
	bool valid = true;

	args.procs_sqrt = sqrt(comm_size);
	if (args.procs_sqrt * args.procs_sqrt != comm_size) {
		if (comm_rank == MASTER_PROC) {
			fprintf(stderr, "Number of processes must be a perfect square\n");
		}

		MPI_Finalize();
		exit(EXIT_FAILURE);
	}

	int ch;
	while ((ch = getopt(argc, argv, "w:h:s:c:x:y:")) != -1) {
		switch (ch) {
		case 'w':
			args.width = stoll(optarg);
			// Need to make sure addition of 2 in width won't overflow
			if (args.width <= 0 || SIZE_MAX - 2 < uintmax_t(args.width)) {
				valid = false;
			}
			break;

		case 'h':
			args.height = stoll(optarg);
			// Need to make sure addition of 2 in height won't overflow
			if (args.height <= 0 || SIZE_MAX - 2 < uintmax_t(args.height)) {
				valid = false;
			}
			break;

		case 's':
			args.steps = stol(optarg);
			if (args.steps <= 0) {
				valid = false;
			}
			break;

		case 'c':
			args.converge_check_every = stol(optarg);
			if (args.converge_check_every <= 0) {
				valid = false;
			}
			break;

		case 'x':
			args.cx = stof(optarg);
			if (!isfinite(args.cx) || args.cx <= 0 || 1 <= args.cx) {
				valid = false;
			}
			break;

		case 'y':
			args.cy = stof(optarg);
			if (!isfinite(args.cy) || args.cy <= 0 || 1 <= args.cy) {
				valid = false;
			}
			break;

		default:
			valid = false;
			break;
		}
	}

	if (!valid) {
		if (comm_rank == MASTER_PROC) {
			fprintf(stderr, "Invalid arguments passed\n");
		}

		MPI_Finalize();
		exit(EXIT_FAILURE);
	}

	if (args.height % args.procs_sqrt != 0 || args.width % args.procs_sqrt != 0) {
		if (comm_rank == MASTER_PROC) {
			fprintf(stderr, "Grid height/width must be evenly distributed between processes\n");
		}

		MPI_Finalize();
		exit(EXIT_FAILURE);
	}

	if (args.height / args.procs_sqrt <= 2 || args.width / args.procs_sqrt <= 2) {
		if (comm_rank == MASTER_PROC) {
			fprintf(stderr, "Per process width/height is too small\n");
		}

		MPI_Finalize();
		exit(EXIT_FAILURE);
	}

	if ((SIZE_MAX / sizeof(float)) / args.height < args.width) {
		if (comm_rank == MASTER_PROC) {
			fprintf(stderr, "Grid size would cause an integer overflow\n");
		}

		MPI_Finalize();
		exit(EXIT_FAILURE);
	}

	return args;
}

MpiAllInfo create_info_object(const Args& args)
{
	MpiAllInfo info;

	info.proc = parsys::map_into_2d_grid(MPI_COMM_WORLD, args.procs_sqrt, args.procs_sqrt);

	info.first = Grid(args.height / args.procs_sqrt + 2, args.width / args.procs_sqrt + 2);
	info.second = Grid(args.height / args.procs_sqrt + 2, args.width / args.procs_sqrt + 2);

	info.cx = args.cx;
	info.cy = args.cy;

	info.steps = args.steps;
	info.converge_check_every = args.converge_check_every;

	MPI_Datatype rowvec;
	MPI_Type_vector(info.first.height() - 2, 1, info.first.width(), MPI_FLOAT, &rowvec);
	MPI_Type_commit(&rowvec);

	std::array<MPI_Request, MAX_REQUESTS> first_send_reqs;
	std::array<MPI_Request, MAX_REQUESTS> first_recv_reqs;

	std::array<MPI_Request, MAX_REQUESTS> second_send_reqs;
	std::array<MPI_Request, MAX_REQUESTS> second_recv_reqs;

	const auto& comm = info.proc.m_comm.get();
	auto col_send_recv_count = info.first.width() - 2;

	auto top_rank = info.proc.m_top_rank;
	if (top_rank != -1) {
		MPI_Send_init(
				&*info.first.begin(1, 1), col_send_recv_count, MPI_FLOAT,
				top_rank, 0, comm, &first_send_reqs[info.neighbor_count]);

		MPI_Send_init(
				&*info.second.begin(1, 1), col_send_recv_count, MPI_FLOAT,
				top_rank, 0, comm, &second_send_reqs[info.neighbor_count]);

		MPI_Recv_init(
				&*info.first.begin(0, 1), col_send_recv_count, MPI_FLOAT,
				top_rank, 0, comm, &first_recv_reqs[info.neighbor_count]);

		MPI_Recv_init(
				&*info.second.begin(0, 1), col_send_recv_count, MPI_FLOAT,
				top_rank, 0, comm, &second_recv_reqs[info.neighbor_count]);

		info.neighbor_count++;
	}

	auto bot_rank = info.proc.m_bot_rank;
	if (bot_rank != -1) {
		auto send_row = info.first.height() - 2;
		auto recv_row = info.first.height() - 1;

		MPI_Send_init(
				&*info.first.begin(send_row, 1), col_send_recv_count, MPI_FLOAT,
				bot_rank, 0, comm, &first_send_reqs[info.neighbor_count]);

		MPI_Send_init(
				&*info.second.begin(send_row, 1), col_send_recv_count, MPI_FLOAT,
				bot_rank, 0, comm, &second_send_reqs[info.neighbor_count]);

		MPI_Recv_init(
				&*info.first.begin(recv_row, 1), col_send_recv_count, MPI_FLOAT,
				bot_rank, 0, comm, &first_recv_reqs[info.neighbor_count]);

		MPI_Recv_init(
				&*info.second.begin(recv_row, 1), col_send_recv_count, MPI_FLOAT,
				bot_rank, 0, comm, &second_recv_reqs[info.neighbor_count]);

		info.neighbor_count++;
	}

	auto left_rank = info.proc.m_left_rank;
	if (left_rank != -1) {
		MPI_Send_init(
				&*info.first.begin(1, 1), 1, rowvec,
				left_rank, 0, comm, &first_send_reqs[info.neighbor_count]);

		MPI_Send_init(
				&*info.second.begin(1, 1), 1, rowvec,
				left_rank, 0, comm, &second_send_reqs[info.neighbor_count]);

		MPI_Recv_init(
				&*info.first.begin(1, 0), 1, rowvec,
				left_rank, 0, comm, &first_recv_reqs[info.neighbor_count]);

		MPI_Recv_init(
				&*info.second.begin(1, 0), 1, rowvec,
				left_rank, 0, comm, &second_recv_reqs[info.neighbor_count]);

		info.neighbor_count++;
	}

	auto right_rank = info.proc.m_right_rank;
	if (right_rank != -1) {
		auto send_col = info.first.width() - 2;
		auto recv_col = info.first.width() - 1;

		MPI_Send_init(
				&*info.first.begin(1, send_col), 1, rowvec,
				right_rank, 0, comm, &first_send_reqs[info.neighbor_count]);

		MPI_Send_init(
				&*info.second.begin(1, send_col), 1, rowvec,
				right_rank, 0, comm, &second_send_reqs[info.neighbor_count]);

		MPI_Recv_init(
				&*info.first.begin(1, recv_col), 1, rowvec,
				right_rank, 0, comm, &first_recv_reqs[info.neighbor_count]);

		MPI_Recv_init(
				&*info.second.begin(1, recv_col), 1, rowvec,
				right_rank, 0, comm, &second_recv_reqs[info.neighbor_count]);

		info.neighbor_count++;
	}

	MPI_Type_free(&rowvec);

	info.first_send_reqs.assign(
			first_send_reqs.begin(), first_send_reqs.begin() + info.neighbor_count);

	info.first_recv_reqs.assign(
			first_recv_reqs.begin(), first_recv_reqs.begin() + info.neighbor_count);

	info.second_send_reqs.assign(
			second_send_reqs.begin(), second_send_reqs.begin() + info.neighbor_count);

	info.second_recv_reqs.assign(
			second_recv_reqs.begin(), second_recv_reqs.begin() + info.neighbor_count);

	return info;
}

Grid generate_grid(size_t height, size_t width)
{
	Grid grid(height, width);

	for (size_t i = 0; i < height ; i++) {
		for (size_t j = 0; j < width; j++) {
			grid(i, j) = (i + 1) * (height - i) * (j + 1) * (width - j);
		}
	}

	return grid;
}

inline float jacobi_iteration(const float* ptr, size_t width, float cx, float cy)
{
	float mid = *ptr;

	float left = *(ptr - 1);
	float right = *(ptr + 1);

	float top = *(ptr - width);
	float bot = *(ptr + width);

	return mid
		+ cy * (top + bot - 2 * mid)
		+ cx * (left + right - 2 * mid);
}

void perform_computation(const Args& args, int size)
{
	MpiAllInfo info = create_info_object(args);

	if (info.proc.m_self_rank == MASTER_PROC) {
		printf("Generating 2D grid...\n");
	}

	auto grid = info.proc.m_self_rank == MASTER_PROC
		? generate_grid(args.height, args.width)
		: Grid();

	array<int, 2> global_dims = {{ int(args.height), int(args.width) }};
	array<int, 2> local_dims = {{
		int(info.first.height() - 2), int(info.first.width() - 2),
	}};
	array<int, 2> starts = {{ 0, 0 }};

	MPI_Datatype array2d;
	MPI_Datatype tmp;
	MPI_Type_create_subarray(global_dims.size(),
			global_dims.data(),
			local_dims.data(),
			starts.data(),
			MPI_ORDER_C,
			MPI_FLOAT,
			&tmp);

	MPI_Type_create_resized(tmp, 0, sizeof(float), &array2d);
	MPI_Type_commit(&array2d);

	MPI_Datatype interior_subarray;
	array<int, 2> exterior_dims = {{
		int(info.first.height()),
			int(info.first.width())
	}};
	array<int, 2> local_starts = {{ 1, 1 }};

	MPI_Type_create_subarray(global_dims.size(),
			exterior_dims.data(),
			local_dims.data(),
			local_starts.data(),
			MPI_ORDER_C,
			MPI_FLOAT,
			&interior_subarray);
	MPI_Type_commit(&interior_subarray);

	vector<int> count(size, 1);
	vector<int> begin_at;

	int procs_sqrt = sqrt(size);

	for (int i = 0; i < procs_sqrt; i++) {
		for (int j = 0; j < procs_sqrt; j++) {
			int idx = i * local_dims[1] * args.height + j * local_dims[1];
			begin_at.push_back(idx);
		}
	}

	if (info.proc.m_self_rank == MASTER_PROC) {
		printf("Writing input file...\n");
		print_grid(grid, "infile.dat");
		printf("Input file was successfully written\n");

		printf("Performing the 2D heat map simulation, this may take a while...\n");
	}

	MPI_Scatterv(&*grid.begin(), count.data(), begin_at.data(), array2d,
			&*info.first.begin(), 1, interior_subarray, MASTER_PROC,
			info.proc.m_comm.get());

	auto width = info.first.width();
	auto height = info.first.height();

	bool use_first = true;

	MPI_Barrier(info.proc.m_comm.get());
	double start = MPI_Wtime();

	long steps;
	for (steps = 0; steps < info.steps; steps++) {
		auto* send_reqs = use_first
			? info.first_send_reqs.data()
			: info.second_send_reqs.data();

		auto* recv_reqs = use_first
			? info.first_recv_reqs.data()
			: info.second_recv_reqs.data();

		const auto& src = use_first
			? info.first
			: info.second;

		auto& dest = use_first
			? info.second
			: info.first;

		MPI_Startall(info.neighbor_count, send_reqs);
		MPI_Startall(info.neighbor_count, recv_reqs);

		bool must_check_for_convergence = steps != 0
			&& steps % info.converge_check_every == 0;

#ifdef _OPENMP
		#pragma omp parallel for collapse(2)
#endif
		for (size_t i = 2; i < height - 2; i++) {
			for (size_t j = 2; j < width - 2; j++) {
				const auto* ptr = &*src.begin(i, j);

				dest(i, j) = jacobi_iteration(ptr, width, info.cx, info.cy);
			}
		}

		MPI_Waitall(info.neighbor_count, recv_reqs, MPI_STATUSES_IGNORE);

		for (size_t i = 1; i < width - 1; i++) {
			const auto* ptr = &*src.begin(1, i);
			dest(1, i) = jacobi_iteration(ptr, width, info.cx, info.cy);
		}

		for (size_t i = 1; i < width - 1; i++) {
			const auto* ptr = &*src.begin(height - 2, i);
			dest(height - 2, i) = jacobi_iteration(ptr, width, info.cx, info.cy);
		}

		for (size_t i = 2; i < height - 2; i++) {
			const auto* ptr = &*src.begin(i, 1);
			dest(i, 1) = jacobi_iteration(ptr, width, info.cx, info.cy);
		}

		for (size_t i = 2; i < height - 2; i++) {
			const auto* ptr = &*src.begin(i, width - 2);
			dest(i, width - 2) = jacobi_iteration(ptr, width, info.cx, info.cy);
		}

		bool locally_converged = true;
		if (must_check_for_convergence) {

#ifdef _OPENMP
			#pragma omp parallel for reduction(&:locally_converged) collapse(2)
#endif
			for (size_t i = 1; i < height - 1; i++) {
				for (size_t j = 1; j < width - 1; j++) {
					locally_converged &= fabs(src(i, j) - dest(i, j)) < FLT_EPSILON;
				}
			}
		}

		MPI_Waitall(info.neighbor_count, send_reqs, MPI_STATUSES_IGNORE);
		use_first = !use_first;

		if (must_check_for_convergence) {
			bool globally_converged = true;
			MPI_Allreduce(&locally_converged, &globally_converged,
					1, MPI_CXX_BOOL, MPI_LAND, info.proc.m_comm.get());

			if (globally_converged) {
				break;
			}
		}
	}

	MPI_Barrier(info.proc.m_comm.get());
	double end = MPI_Wtime();

	if (info.proc.m_self_rank == MASTER_PROC) {
		printf("Time taken for %ld steps: %.3f sec\n", steps, end - start);
	}

	const auto* output_buf = use_first
		? &*info.first.begin()
		: &*info.second.begin();

	MPI_Gatherv(output_buf, 1, interior_subarray,
			&*grid.begin(), count.data(), begin_at.data(), array2d, MASTER_PROC,
			info.proc.m_comm.get());

	MPI_Type_free(&array2d);
	MPI_Type_free(&interior_subarray);

	if (info.proc.m_self_rank == MASTER_PROC) {
		printf("Writing output file...\n");
		print_grid(grid, "outfile.dat");
		printf("Output file was successfully written\n");
	}
}

} // namespace

int main(int argc, char** argv)
{
	int mpi_thread_support;

	MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &mpi_thread_support);

	int rank;
	int size;

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	if (mpi_thread_support != MPI_THREAD_FUNNELED) {
		if (rank == MASTER_PROC) {
			fprintf(stderr, "Single threaded MPI implementations are unsupported\n");
		}
		return EXIT_FAILURE;
	}

	Args args = parse_args(argc, argv, rank, size);
	perform_computation(args, size);

	MPI_Finalize();
	return EXIT_SUCCESS;
}
