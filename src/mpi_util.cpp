#include "mpi_util.h"

#include <mpi.h>

#include <array>
#include <cstdio>

using std::array;

using parsys::MpiProc;

MpiProc parsys::map_into_2d_grid(MPI_Comm comm, int rows, int cols)
{
	array<int, 2> dims = {{rows, cols}};
	array<int, 2> periods = {{0, 0}};

	MPI_Comm new_comm;

	int err = MPI_Cart_create(comm, dims.size(), dims.data(), periods.data(), true, &new_comm);
	if (err != MPI_SUCCESS) {
		MPI_Abort(comm, err);
	}

	int rank;
	MPI_Comm_rank(new_comm, &rank);

	int top_rank = -1;
	int bot_rank = -1;
	err = MPI_Cart_shift(new_comm, 0, 1, &top_rank, &bot_rank);
	if (err != MPI_SUCCESS) {
		MPI_Abort(comm, err);
	}

	int left_rank = -1;
	int right_rank = -1;
	err = MPI_Cart_shift(new_comm, 1, 1, &left_rank, &right_rank);
	if (err != MPI_SUCCESS) {
		MPI_Abort(comm, err);
	}

	return MpiProc(new_comm, rank, top_rank, bot_rank, left_rank, right_rank);
}
