#pragma once

#include "grid.h"

#include <mpi.h>

#include <array>
#include <utility>

constexpr int MPI_NO_NEIGHBOR = -1;

constexpr size_t MAX_REQUESTS = 4;

namespace parsys {

/**
 * Thin wrapper around an MPI communicator handle. All operations performed
 * on it will not throw an exception.
 */
class MpiCommunicator
{
	private:
		MPI_Comm m_comm;

		explicit MpiCommunicator(MPI_Comm comm) noexcept
			: m_comm(comm)
		{
		}

	public:
		/**
		 * Default constructor. Defaults to world communicator.
		 */
		MpiCommunicator() noexcept
			: m_comm(MPI_COMM_WORLD)
		{
		}

		~MpiCommunicator()
		{
			reset();
		}

		/**
		 * Copy construction is disallowed.
		 */
		MpiCommunicator(const MpiCommunicator&) = delete;

		/**
		 * Copy assignment is disallowed.
		 */
		MpiCommunicator& operator=(const MpiCommunicator&) = delete;

		/**
		 * Move constructor.
		 */
		MpiCommunicator(MpiCommunicator&& other) noexcept
			: m_comm(other.m_comm)
		{
			other.m_comm = MPI_COMM_NULL;
		}

		/**
		 * Move assignment operator.
		 */
		MpiCommunicator& operator=(MpiCommunicator&& other) noexcept
		{
			reset();
			m_comm = other.m_comm;
			other.m_comm = MPI_COMM_NULL;

			return *this;
		}

		/**
		 * Swap two communicators.
		 */
		void swap(MpiCommunicator& rhs) noexcept
		{
			std::swap(m_comm, rhs.m_comm);
		}

		/**
		 * Return the underlying communicator handle (const version).
		 */
		const MPI_Comm& get() const noexcept
		{
			return m_comm;
		}

		/**
		 * Return the underlying communicator handle (non const version).
		 */
		MPI_Comm& get() noexcept
		{
			return m_comm;
		}

		/**
		 * Destroy the existing communicator, if necessary, then set it to the
		 * null communicator.
		 */
		void reset() noexcept
		{
			switch (m_comm) {
			case MPI_COMM_NULL:
			case MPI_COMM_WORLD:
			case MPI_COMM_SELF:
				m_comm = MPI_COMM_NULL;

				return;
			}

			// MPI standard guarantees handle will be null
			MPI_Comm_free(&m_comm);
		}

		/**
		 * Returns the communicator's size or `-1` if the communicator is the
		 * null communicator.
		 *
		 * No caching is performed on the communicator's size value; calling
		 * this function repeatedly may be an overhead.
		 */
		int size() const noexcept
		{
			if (m_comm == MPI_COMM_NULL) {
				return -1;
			}

			int result;
			MPI_Comm_size(m_comm, &result);

			return result;
		}

		/**
		 * Returns the rank of the calling process for this communicator or
		 * `-1` if the communicator is the null communicator.
		 *
		 * No caching is performed on the rank value; calling this function
		 * repeatedly may be an overhead.
		 */
		int rank() const noexcept
		{
			if (m_comm == MPI_COMM_NULL) {
				return -1;
			}

			int result;
			MPI_Comm_rank(m_comm, &result);

			return result;
		}

		/**
		 * Returns a communicator that takes ownership of the given handle.
		 *
		 * This means that when the communicator falls out of scope, the handle
		 * will be released, unless it's `MPI_COMM_NULL`, `MPI_COMM_WORLD` or
		 * `MPI_COMM_SELF`.
		 */
		static MpiCommunicator take_ownership(MPI_Comm comm) noexcept
		{
			return MpiCommunicator(comm);
		}
};

class MpiRequestArray
{
	private:
		std::array<MPI_Request, MAX_REQUESTS> m_reqs;

	public:
		MpiRequestArray() noexcept
		{
			m_reqs.fill(MPI_REQUEST_NULL);
		}

		template<typename Iter>
		MpiRequestArray(Iter begin, Iter end)
		{
			m_reqs.fill(MPI_REQUEST_NULL);
			auto actual_end = std::distance(begin, end) > 4
				? begin + 4
				: end;

			std::copy(begin, actual_end, m_reqs.begin());
		}

		MpiRequestArray(const MpiRequestArray&) = delete;
		MpiRequestArray& operator=(const MpiRequestArray&) = delete;

		MpiRequestArray(MpiRequestArray&& other) noexcept
		{
			std::copy(other.m_reqs.begin(), other.m_reqs.end(), m_reqs.begin());
			other.m_reqs.fill(MPI_REQUEST_NULL);
		}

		MpiRequestArray& operator=(MpiRequestArray&& other) noexcept
		{
			reset();

			std::copy(other.m_reqs.begin(), other.m_reqs.end(), m_reqs.begin());
			other.m_reqs.fill(MPI_REQUEST_NULL);

			return *this;
		}

		void reset() noexcept
		{
			for (auto& it : m_reqs) {
				if (it != MPI_REQUEST_NULL) {
					MPI_Request_free(&it);
					it = MPI_REQUEST_NULL;
				}
			}
		}

		template<typename Iter>
		void assign(Iter begin, Iter end)
		{
			reset();
			auto actual_end = std::distance(begin, end) > 4
				? begin + 4
				: end;

			std::copy(begin, actual_end, m_reqs.begin());
		}

		MPI_Request* data() noexcept
		{
			return m_reqs.data();
		}

		const MPI_Request* data() const noexcept
		{
			return m_reqs.data();
		}

		~MpiRequestArray()
		{
			reset();
		}
};

struct MpiProc
{
	MpiCommunicator m_comm;

	int m_self_rank;

	int m_top_rank;
	int m_bot_rank;
	int m_left_rank;
	int m_right_rank;

	MpiProc() noexcept
		: m_comm()
		, m_self_rank(0)
		, m_top_rank(0)
		, m_bot_rank(0)
		, m_left_rank(0)
		, m_right_rank(0)
	{
	}

	MpiProc(MPI_Comm comm, int self, int top, int bot, int left, int right) noexcept
		: m_comm(MpiCommunicator::take_ownership(comm))
		, m_self_rank(self)
		, m_top_rank(top)
		, m_bot_rank(bot)
		, m_left_rank(left)
		, m_right_rank(right)
	{
	}
};

MpiProc map_into_2d_grid(MPI_Comm comm, int rows, int cols);

} // namespace parsys
