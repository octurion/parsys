#pragma once

#include <cstdint>
#include <climits>
#include <cstddef>
#include <stdexcept>
#include <vector>
#include <utility>

class Grid
{
	std::vector<float> m_grid;

	size_t m_width;
	size_t m_height;

	public:
		typedef std::vector<float>::value_type      value_type;
		typedef std::vector<float>::reference       reference;
		typedef std::vector<float>::const_reference const_reference;
		typedef std::vector<float>::iterator        iterator;
		typedef std::vector<float>::const_iterator  const_iterator;
		typedef std::vector<float>::difference_type difference_type;
		typedef std::vector<float>::size_type       size_type;

		Grid() noexcept
			: m_grid()
			, m_width(0)
			, m_height(0)
		{
		}

		Grid(const Grid&) = delete;
		Grid& operator=(const Grid&) = delete;

		Grid(Grid&&) = default;
		Grid& operator=(Grid&&) = default;

		Grid(size_t height, size_t width)
			: m_grid()
			, m_width(width)
			, m_height(height)
		{
			if (width == 0 || height == 0 || SIZE_MAX / width < height) {
				throw std::invalid_argument("Invalid dimensions passed to grid");
			}

			m_grid.assign(width * height, 0);
		}

		size_t width() const noexcept
		{
			return m_width;
		}

		size_t height() const noexcept
		{
			return m_height;
		}

		size_t size() const noexcept
		{
			return m_grid.size();
		}

		bool empty() const noexcept
		{
			return m_grid.empty();
		}

		iterator begin() noexcept
		{
			return m_grid.begin();
		}

		iterator end() noexcept
		{
			return m_grid.end();
		}

		const_iterator begin() const noexcept
		{
			return m_grid.begin();
		}

		const_iterator end() const noexcept
		{
			return m_grid.end();
		}

		const_iterator cbegin() const noexcept
		{
			return m_grid.begin();
		}

		const_iterator cend() const noexcept
		{
			return m_grid.end();
		}

		iterator begin(size_t row, size_t col) noexcept
		{
			return m_grid.begin() + row * m_width + col;
		}

		iterator end(size_t row, size_t col) noexcept
		{
			return begin(row, col);
		}

		const_iterator begin(size_t row, size_t col) const noexcept
		{
			return m_grid.begin() + row * m_width + col;
		}

		const_iterator end(size_t row, size_t col) const noexcept
		{
			return begin(row, col);
		}

		const_iterator cbegin(size_t row, size_t col) const noexcept
		{
			return m_grid.cbegin() + row * m_width + col;
		}

		const_iterator cend(size_t row, size_t col) const noexcept
		{
			return cbegin(row, col);
		}

		float& operator()(size_t row, size_t col)
		{
			return m_grid[row * m_width + col];
		}

		const float& operator()(size_t row, size_t col) const
		{
			return m_grid[row * m_width + col];
		}

		void swap(Grid& other) noexcept
		{
			m_grid.swap(other.m_grid);
			std::swap(m_width, other.m_width);
			std::swap(m_height, other.m_height);
		}
};
